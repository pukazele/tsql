GO
--1
SELECT P.Position,
COUNT(EP.EmployeeId) AS 'Count'
FROM POSITIONS AS P
LEFT JOIN EMPLOYEES_PROJECTS AS EP
ON P.Id = EP.PositionId
GROUP BY P.Position

GO
--2
SELECT DISTINCT(P.Position)
FROM POSITIONS AS P
EXCEPT
SELECT DISTINCT(P.Position)
FROM EMPLOYEES_PROJECTS AS EP
JOIN POSITIONS AS P
ON EP.PositionId = P.Id

GO
--3
SELECT Proj.Name,
Pos.Position,
COUNT(EP.EmployeeId) AS 'Count'
FROM EMPLOYEES_PROJECTS AS EP
JOIN PROJECTS AS Proj
ON EP.ProjId = Proj.Id
JOIN POSITIONS AS Pos
ON EP.PositionId = Pos.Id
GROUP BY Proj.Name, Pos.Position
ORDER BY Proj.Name

GO
--4
SELECT Proj.Name,
COUNT(EP.EmployeeId) / COUNT(T.Id)
FROM TASKS AS T
RIGHT JOIN EMPLOYEES_PROJECTS AS EP
ON EP.EmployeeId = T.EmployeeId AND EP.ProjId = T.ProjId
LEFT JOIN PROJECTS AS Proj
ON T.ProjId = Proj.Id
WHERE Proj.Id IS NOT NULL
GROUP BY Proj.Name

GO
--5
SELECT CAST(DATEDIFF(MONTH, P.StartDate, P.EndDate) AS VARCHAR) + ' months' AS Duration
FROM PROJECTS AS P

GO
--6

DECLARE @min INT

SELECT @min = MIN(S.Count)
FROM(SELECT 
COUNT(NotSolved.Name) AS Count,
NotSolved.FirstName,
NotSolved.LastName
FROM (SELECT E.FirstName,
	  E.LastName,
	  T.Name
	  FROM TASKS AS T
	  RIGHT JOIN EMPLOYEES_PROJECTS AS EP
	  ON T.EmployeeId = EP.EmployeeId AND EP.ProjId = T.ProjId
	  JOIN EMPLOYEES AS E
	  ON T.EmployeeId = E.Id
	  JOIN TASKS_STATUSES AS TS
	  ON T.StatusId = TS.Id
	  GROUP BY E.FirstName, E.LastName, TS.Id, T.Name
	  HAVING TS.Id != 4) AS NotSolved
GROUP BY NotSolved.FirstName, NotSolved.LastName) AS S

SELECT *
FROM(SELECT 
COUNT(NotSolved.Name) AS Count,
NotSolved.FirstName,
NotSolved.LastName
FROM (SELECT E.FirstName,
	  E.LastName,
	  T.Name
	  FROM TASKS AS T
	  RIGHT JOIN EMPLOYEES_PROJECTS AS EP
	  ON T.EmployeeId = EP.EmployeeId AND EP.ProjId = T.ProjId
	  JOIN EMPLOYEES AS E
	  ON T.EmployeeId = E.Id
	  JOIN TASKS_STATUSES AS TS
	  ON T.StatusId = TS.Id
	  GROUP BY E.FirstName, E.LastName, TS.Id, T.Name
	  HAVING TS.Id != 4) AS NotSolved
GROUP BY NotSolved.FirstName, NotSolved.LastName) AS S
WHERE S.Count = @min

GO
--7
DECLARE @max INT,
@currentDate DATE

SELECT @currentDate = CONVERT (date, SYSDATETIME())


SELECT @max = MAX(S.Count)
FROM(SELECT 
COUNT(NotSolved.Name) AS Count,
NotSolved.FirstName,
NotSolved.LastName
FROM (SELECT E.FirstName,
	  E.LastName,
	  T.Name
	  FROM TASKS AS T
	  RIGHT JOIN EMPLOYEES_PROJECTS AS EP
	  ON T.EmployeeId = EP.EmployeeId AND EP.ProjId = T.ProjId
	  JOIN EMPLOYEES AS E
	  ON T.EmployeeId = E.Id
	  JOIN TASKS_STATUSES AS TS
	  ON T.StatusId = TS.Id
	  GROUP BY E.FirstName, E.LastName, TS.Id, T.Name
	  HAVING TS.Id != 4) AS NotSolved
GROUP BY NotSolved.FirstName, NotSolved.LastName) AS S


