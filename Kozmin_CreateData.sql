USE [Task_1]
GO
SET IDENTITY_INSERT [dbo].[PROJECTS] ON 

INSERT [dbo].[PROJECTS] ([Id], [Name], [StartDate], [EndDate], [Status]) VALUES (1, N'Project 1', CAST(N'2019-04-20' AS Date), CAST(N'2022-05-14' AS Date), N'Open')
INSERT [dbo].[PROJECTS] ([Id], [Name], [StartDate], [EndDate], [Status]) VALUES (3, N'Project 2', CAST(N'2017-05-11' AS Date), CAST(N'2018-03-18' AS Date), N'Closed')
INSERT [dbo].[PROJECTS] ([Id], [Name], [StartDate], [EndDate], [Status]) VALUES (4, N'Project 3', CAST(N'2015-10-12' AS Date), CAST(N'2017-06-27' AS Date), N'Closed')
INSERT [dbo].[PROJECTS] ([Id], [Name], [StartDate], [EndDate], [Status]) VALUES (5, N'Project 4', CAST(N'2020-11-21' AS Date), CAST(N'2021-04-01' AS Date), N'Open')
INSERT [dbo].[PROJECTS] ([Id], [Name], [StartDate], [EndDate], [Status]) VALUES (6, N'Project 5', CAST(N'2021-03-15' AS Date), CAST(N'2021-04-15' AS Date), N'Open')
SET IDENTITY_INSERT [dbo].[PROJECTS] OFF
GO
SET IDENTITY_INSERT [dbo].[EMPLOYEES] ON 

INSERT [dbo].[EMPLOYEES] ([Id], [FirstName], [LastName]) VALUES (1, N'Yevhenii', N'Kozmin')
INSERT [dbo].[EMPLOYEES] ([Id], [FirstName], [LastName]) VALUES (2, N'Vitaliy', N'Cal')
INSERT [dbo].[EMPLOYEES] ([Id], [FirstName], [LastName]) VALUES (3, N'Vladimir', N'Parerin')
INSERT [dbo].[EMPLOYEES] ([Id], [FirstName], [LastName]) VALUES (4, N'Yurii', N'Sabadosh')
INSERT [dbo].[EMPLOYEES] ([Id], [FirstName], [LastName]) VALUES (5, N'Vladislava', N'Datsenko')
INSERT [dbo].[EMPLOYEES] ([Id], [FirstName], [LastName]) VALUES (6, N'Shkuratiuk', N'Kirill')
INSERT [dbo].[EMPLOYEES] ([Id], [FirstName], [LastName]) VALUES (7, N'Tiushkevich', N'Dmitrii')
INSERT [dbo].[EMPLOYEES] ([Id], [FirstName], [LastName]) VALUES (8, N'Daniil', N'Liguta')
INSERT [dbo].[EMPLOYEES] ([Id], [FirstName], [LastName]) VALUES (9, N'Nikifor', N'Funtusov')
SET IDENTITY_INSERT [dbo].[EMPLOYEES] OFF
GO
SET IDENTITY_INSERT [dbo].[POSITIONS] ON 

INSERT [dbo].[POSITIONS] ([Id], [Position]) VALUES (2, N'Developer')
INSERT [dbo].[POSITIONS] ([Id], [Position]) VALUES (3, N'Project Manager')
INSERT [dbo].[POSITIONS] ([Id], [Position]) VALUES (1, N'Team Lead')
SET IDENTITY_INSERT [dbo].[POSITIONS] OFF
GO
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (1, 2, 1)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (1, 4, 2)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (1, 5, 2)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (1, 3, 3)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (3, 7, 1)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (3, 2, 2)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (3, 3, 3)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (3, 6, 2)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (4, 4, 1)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (4, 8, 2)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (4, 7, 3)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (5, 9, 1)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (5, 6, 2)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (6, 8, 2)
INSERT [dbo].[EMPLOYEES_PROJECTS] ([ProjId], [EmployeeId], [PositionId]) VALUES (6, 6, 3)
GO
SET IDENTITY_INSERT [dbo].[TASKS_STATUSES] ON 

INSERT [dbo].[TASKS_STATUSES] ([Id], [Status]) VALUES (3, N'Accepted')
INSERT [dbo].[TASKS_STATUSES] ([Id], [Status]) VALUES (1, N'Open')
INSERT [dbo].[TASKS_STATUSES] ([Id], [Status]) VALUES (2, N'Requires improvement')
INSERT [dbo].[TASKS_STATUSES] ([Id], [Status]) VALUES (4, N'Solved')
SET IDENTITY_INSERT [dbo].[TASKS_STATUSES] OFF
GO
SET IDENTITY_INSERT [dbo].[TASKS] ON 

INSERT [dbo].[TASKS] ([Id], [ProjId], [Name], [Deadline], [EmployeeId], [StatusId], [StatusDate], [EmployeeThatChangedStatusId]) VALUES (1, 1, N'Task 1', CAST(N'2021-04-03' AS Date), 2, 3, CAST(N'2021-03-29' AS Date), 2)
INSERT [dbo].[TASKS] ([Id], [ProjId], [Name], [Deadline], [EmployeeId], [StatusId], [StatusDate], [EmployeeThatChangedStatusId]) VALUES (2, 1, N'Task 2', CAST(N'2021-05-05' AS Date), 5, 2, CAST(N'2021-04-04' AS Date), 2)
INSERT [dbo].[TASKS] ([Id], [ProjId], [Name], [Deadline], [EmployeeId], [StatusId], [StatusDate], [EmployeeThatChangedStatusId]) VALUES (4, 3, N'Task 3', CAST(N'2018-02-20' AS Date), 3, 4, CAST(N'2018-02-19' AS Date), 3)
INSERT [dbo].[TASKS] ([Id], [ProjId], [Name], [Deadline], [EmployeeId], [StatusId], [StatusDate], [EmployeeThatChangedStatusId]) VALUES (5, 4, N'Task 4', CAST(N'2016-04-12' AS Date), 8, 4, CAST(N'2016-04-10' AS Date), 8)
INSERT [dbo].[TASKS] ([Id], [ProjId], [Name], [Deadline], [EmployeeId], [StatusId], [StatusDate], [EmployeeThatChangedStatusId]) VALUES (6, 4, N'Task 5', CAST(N'2017-01-01' AS Date), 7, 4, CAST(N'2017-01-01' AS Date), 7)
INSERT [dbo].[TASKS] ([Id], [ProjId], [Name], [Deadline], [EmployeeId], [StatusId], [StatusDate], [EmployeeThatChangedStatusId]) VALUES (7, 5, N'Task 6', CAST(N'2021-03-29' AS Date), NULL, 1, CAST(N'2021-03-21' AS Date), 9)
INSERT [dbo].[TASKS] ([Id], [ProjId], [Name], [Deadline], [EmployeeId], [StatusId], [StatusDate], [EmployeeThatChangedStatusId]) VALUES (8, 6, N'Task 7', CAST(N'2021-04-10' AS Date), 6, 2, CAST(N'2021-03-23' AS Date), 6)
SET IDENTITY_INSERT [dbo].[TASKS] OFF
GO
